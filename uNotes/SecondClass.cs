﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using FluentFTP;

namespace uNotes
{
    public partial class SecondClass : Form
    {
        public SecondClass()
        {
            InitializeComponent();
        }

        public FtpClient kliens = new FtpClient(); //Itt példányosítom az FTP klienst
        public string selectedFolder = "";
        public string selectedItem = "";




        private void SecondClass_Load(object sender, EventArgs e)
        {
            kliens.Host = "ftp.cardio.pe.hu";
            kliens.Credentials = new NetworkCredential("u824252153", "carter111");
            //csatlakozási adatok megadása
        }

        private void button1_Click(object sender, EventArgs e)
        {
            kliens.Connect();
            //kliens.CreateDirectory("NoooNeszeee/testMappa1/TestFolder2");
            string filePath = @"C:/SYNTAXSCORE/";
            string remotePath = @"Data/Upload/";

            string fileName = "test";
            string remoteFileName = fileName;

            string fileExt = ".txt";
            string remoteFileExt = fileExt;

            kliens.UploadFile(filePath + fileName + fileExt, remotePath + remoteFileName + remoteFileExt, FtpExists.NoCheck, true);
            MessageBox.Show("'" + fileName + fileExt + "'" + " successfully uploaded to FTP server!");
        }
        private void SecondClass_FormClosing(object sender, FormClosingEventArgs e)
        {
            kliens.Dispose();
        }
        
        
        private void button2_Click(object sender, EventArgs e)
        {
            fillListBoxes(selectedFolder);
        }
        private void fillListBoxes(string path)
        {
            listBox1.Items.Clear();   //TABULA RASA
            listBox2.Items.Clear();

            listBox1.Items.Add("..");
            //listBox2.Items.Add("..");

            kliens.Connect();

            FtpListItem[] FTPList = kliens.GetListing(path);
            string folderName;
            foreach (FtpListItem elem in FTPList)
            {
                if (elem.Type == FtpFileSystemObjectType.Directory)
                {
                    folderName = elem.Name;
                    listBox1.Items.Add(folderName);
                }
                if (elem.Type == FtpFileSystemObjectType.File)
                {
                    folderName = elem.Name;
                    listBox2.Items.Add(folderName);
                }
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (listBox1.SelectedItem.ToString() != "..")
            {
                selectedFolder += listBox1.SelectedItem.ToString() + @"/";
                //MessageBox.Show(selectedFolder);
                fillListBoxes(selectedFolder);
            }
            else
            {
                string currentFolder = selectedFolder;
                selectedFolder = moveUpPath(currentFolder);
                fillListBoxes(selectedFolder);
            }
            MessageBox.Show(selectedFolder);
        }
        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedItem = listBox2.SelectedItem.ToString();
        }
        private string moveUpPath(string path)
        {
            string[] explString = path.Split('/');
            int counter = 0;
            string newPath = "";
            foreach (string elem in explString)
            {
                counter++;
                if (counter < explString.Length - 1)
                {
                    newPath += elem + '/';
                }
            }
            return newPath;
        }

        private void DownloadFile(string remoteFilePath, string remoteFileName)
        {
            kliens.Connect();
            string localPath = @"C:\uNotes\" + remoteFileName;
            string remotePath = "";

            if (remoteFilePath != "")
            {
                remotePath = remoteFilePath + remoteFileName;
            }
            else
            {
                remotePath = remoteFileName;
            }

            MessageBox.Show(localPath + " remote: " + remotePath);
            kliens.DownloadFile(localPath, remotePath, true);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DownloadFile(selectedFolder, selectedItem);
        }

       
    }
}
