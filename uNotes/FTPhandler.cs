﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using FluentFTP;

namespace uNotes
{
    class FTPhandler
    {
        public FtpClient kliens = new FtpClient(); //Itt példányosítom az FTP klienst
        private void ConnectToFtp()
        {
            kliens.Host = "ftp.cardio.pe.hu";
            kliens.Credentials = new NetworkCredential("u824252153", "carter111");
            kliens.Connect();
        }

        private void Disconnect()
        {
            kliens.Disconnect();
        }
        public List<String> GetFolders(string path)
        {
            ConnectToFtp();
            FtpListItem[] FTPList = kliens.GetListing(path);
            string folderName;
            List<String> folders = new List<String>();
            foreach (FtpListItem elem in FTPList)
            {
                if (elem.Type == FtpFileSystemObjectType.Directory)
                {
                    folderName = elem.Name;
                    folders.Add(folderName);
                }
            }
            kliens.Disconnect();
            return folders;
        }

        public List<String> GetFiles(string path)
        {
            ConnectToFtp();
            FtpListItem[] FTPList = kliens.GetListing(path);
            string fileName;
            List<String> files = new List<String>();
            foreach (FtpListItem elem in FTPList)
            {
                if (elem.Type == FtpFileSystemObjectType.File)
                {
                    fileName = elem.Name;
                    files.Add(fileName);
                }
            }
            kliens.Disconnect();
            return files;
        }

    }
}
