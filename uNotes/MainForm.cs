﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FluentFTP;
using System.Net;
using System.IO;

namespace uNotes
{
    
    public partial class MainForm : Form
    {
        
        public MainForm()
        {

            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            kliens.Host = "ftp.cardio.pe.hu";
            kliens.Credentials = new NetworkCredential("u824252153", "carter111");
            UpdateNewsFeed();
            splitContainer1.SplitterDistance = (int)(splitContainer1.Width * 0.20); //SplitContainer két panele közti arány beállítása
            tableLayoutPanel2.Visible = true;
            toolStripLabel2.Text = "Data/DOTE/ÁOK/";
        }

        public static bool loggedIn = false;
        public static string loggedInAs;

        public FtpClient kliens = new FtpClient(); //Itt példányosítom az FTP klienst
       
        private void anyad()
        {
            toolStripLabel1.Text = loggedInAs;
        }
        public static void getAccessLevel(string _username)
        {

        }
        private void button4_Click(object sender, EventArgs e)
        {
           
        }

        private void toolStripContainer1_TopToolStripPanel_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        
        private void UpdateNewsFeed()
        {
            webBrowser1.DocumentText =
    "<html><body><br><br>Please enter your name:<br/>" +
    "<input type='text' name='userName'/><br/>" +
    "<a href='http://www.microsoft.com'>continue</a>" +
    "</body></html>";
            string url = @"https://tamasbenoecs.wixsite.com/mysite";
            webBrowser1.Navigate("https://tamasbenoecs.wixsite.com/mysite");
        }
        private void toolStripContainer1_ContentPanel_Load(object sender, EventArgs e)
        {

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void profileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Profile profil = new Profile();
            profil.Show();
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (loggedIn)
            {
                loggedIn = false;
                MessageBox.Show("Succesfully logged out.");
                loginToolStripMenuItem.Text = "Login";
            }
            else
            {
                Login loginScreen = new Login();
                loginScreen.Show();
            }

           
        }

        public void updateLoginBtnText()
        {
            if(loggedIn)
                loginToolStripMenuItem.Text = "Logout";
            else
                loginToolStripMenuItem.Text = "Login";
        }

        private void downloadsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SecondClass ftpBrowserWindow = new SecondClass();
            ftpBrowserWindow.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            UpdateNewsFeed();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Profile profil = new Profile();
            profil.Show();
        }

        private void toolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip1.Visible = !toolStrip1.Visible;

        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            updateLoginBtnText();
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {
            toolStripLabel1.Text = loggedInAs;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tableLayoutPanel2.Visible = !tableLayoutPanel2.Visible;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Label text = new Label();
            text.Text = "ANYAD!";
            
            tableLayoutPanel2.Controls.Add(text);
        }

        private void UpdateFileHandler(string path)
        {
            tableLayoutPanel2.Controls.Clear();
            toolStripLabel2.Text = path;
            FTPhandler ftp = new FTPhandler();
            string _path = path;
            //string nyenye; //DEBUG
            List<String> folders = ftp.GetFolders(_path);
            if(folders.Count == 0) // ha üres a mappa, lekérdezi a fájlokat
            {
                List<String> files = ftp.GetFiles(_path);
                foreach (String item in files) //DEBUG
                {
                    //Console.WriteLine(item); //DEBUG remove

                    Label cimke = new Label(); // Label beállításai
                    cimke.Text = item;
                    cimke.Name = item;
                    Font betu = new Font(FontFamily.GenericSansSerif, 12f);
                    cimke.Font = betu;
                    cimke.Click += new EventHandler(clickEventDownload);
                    cimke.Dock = DockStyle.Fill;
                    cimke.TextAlign = ContentAlignment.MiddleCenter;

                    tableLayoutPanel2.Controls.Add(cimke);//A kész sablonok hozzáadása
                }
            }
            else
            {
                foreach (String item in folders) //DEBUG
                {
                    //Console.WriteLine(item); //DEBUG remove

                    Label cimke = new Label(); // Label beállításai
                    cimke.Text = item;
                    cimke.Name = item;
                    Font betu = new Font(FontFamily.GenericSansSerif, 12f);
                    cimke.Font = betu;
                    cimke.Click += new EventHandler(clickEvent2);
                    cimke.Dock = DockStyle.Fill;
                    cimke.TextAlign = ContentAlignment.MiddleCenter;

                    PictureBox picBox = new PictureBox();  // picbox beállításai
                    picBox.ImageLocation = @"Resources\FolderR2.PNG";
                    picBox.Height = 200;
                    picBox.Width = 200;
                    picBox.Name = cimke.Text;
                    picBox.Click += new EventHandler(clickEvent);


                    tableLayoutPanel2.Controls.Add(picBox);   //A kész sablonok hozzáadása
                    tableLayoutPanel2.Controls.Add(cimke);
                }
            }
            

            
            
        }

        void clickEvent(object sender, EventArgs e)
        {
            PictureBox felado = (PictureBox)sender;
            UpdateFileHandler(toolStripLabel2.Text + felado.Name + "/");
        }

        void clickEvent2(object sender, EventArgs e)
        {
            Label felado = (Label)sender;
            UpdateFileHandler(toolStripLabel2.Text + felado.Name + "/");
        }

        void clickEventDownload(object sender, EventArgs e)
        {
            Label felado = (Label)sender;
            string localPath = @"C:\uNotes\" + felado.Name;
            string remoteFilePath = toolStripLabel2.Text;
            string remoteFileName = felado.Name;
            string remoteFile = "";

            if (remoteFilePath != "")
            {
                remoteFile = remoteFilePath + "/" + remoteFileName;
            }
            else
            {
                remoteFile = remoteFileName;
            }

            //MessageBox.Show(localPath + " remote: " + remoteFile);
            kliens.DownloadFile(localPath, remoteFile, true);
            MessageBox.Show("A fájl sikeresen letöltve!");
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            UpdateFileHandler(@"Data/DOTE/ÁOK/");
        }

        private void toolStripLabel3_Click(object sender, EventArgs e)
        {
            string path = toolStripLabel2.Text;
            string path_new = "";
            String[] pathFragments = path.Split('/');
            for (int i = 0; i < pathFragments.Length - 2; i++)
            {
                path_new += pathFragments[i] + "/";
                //MessageBox.Show(newPath);
            }
            UpdateFileHandler(path_new);
        }
    }
}
