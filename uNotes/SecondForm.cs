﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using FluentFTP;

namespace uNotes
{
    public partial class SecondClass : Form
    {
        public SecondClass()
        {
            InitializeComponent();
        }

        public FtpClient kliens = new FtpClient(); //Itt példányosítom az FTP klienst

        public string g_Path = "";




        private void SecondClass_Load(object sender, EventArgs e)
        {
            kliens.Host = "ftp.cardio.pe.hu";
            kliens.Credentials = new NetworkCredential("u824252153", "carter111");
            //csatlakozási adatok megadása
        }

        private void button1_Click(object sender, EventArgs e)
        {
            kliens.Connect();
            //kliens.CreateDirectory("NoooNeszeee/testMappa1/TestFolder2");
            string filePath = @"C:/SYNTAXSCORE/";
            string remotePath = @"Data/Upload/";

            string fileName = "test";
            string remoteFileName = fileName;

            string fileExt = ".txt";
            string remoteFileExt = fileExt;

            //kliens.UploadFile(filePath + fileName + fileExt, remotePath + remoteFileName + remoteFileExt, FtpExists.NoCheck, true);
            //MessageBox.Show("'" + fileName + fileExt + "'" + " successfully uploaded to FTP server!");
        }
        private void button2_Click(object sender, EventArgs e)
        {
            UpdateListBoxes();
        }  
        private void UpdateListBoxes()
        {
            listBox1.Items.Clear();   //TABULA RASA
            listBox2.Items.Clear();

            listBox1.Items.Add("..");
            //listBox2.Items.Add("..");

          
          
            FtpListItem[] FTPList = kliens.GetListing(g_Path);
            string folderName;
            foreach (FtpListItem elem in FTPList)
            {
                if (elem.Type == FtpFileSystemObjectType.Directory)
                {
                    folderName = elem.Name;
                    listBox1.Items.Add(folderName);
                }
                if (elem.Type == FtpFileSystemObjectType.File)
                {
                    folderName = elem.Name;
                    listBox2.Items.Add(folderName);
                }
            }
        }   
        private void SecondClass_FormClosing(object sender, FormClosingEventArgs e)
        {
            kliens.Dispose();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            DownloadFile(g_Path, GetSelectedItemListBox2());
        }
        private void listBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if(GetSelectedItemListBox1() != "..")
                {
                    addItemToCurrentPath(GetSelectedItemListBox1());
                }
                else
                {
                    moveUpTree();
                }
                
            }
            if(e.KeyCode == Keys.Back)
            {
                moveUpTree();
            }
        }
        private void listBox1DblClick(object sender, EventArgs e)
        {
            if (GetSelectedItemListBox1() != "..")
            {
                addItemToCurrentPath(GetSelectedItemListBox1());
            }
            else
            {
                moveUpTree();
            }
        }
        private void addItemToCurrentPath(string _selectedItem)
        {
            g_Path += _selectedItem + "/";
            UpdatePath();
            UpdateListBoxes();
        }
        private void moveUpTree()
        {
            string newPath = "";
            string[] pathExpl = g_Path.Split('/');

            for (int i = 0; i <  pathExpl.Length - 2; i++)
            {
                newPath += pathExpl[i] + "/";
                //MessageBox.Show(newPath);
            }
            g_Path = newPath;
            UpdatePath();
            UpdateListBoxes();
        }
        private string GetSelectedItemListBox1()
        {
            return listBox1.SelectedItem.ToString();
        }
        private string GetSelectedItemListBox2()
        {
           // GetSelectedItemListBox2();
            return listBox2.SelectedItem.ToString();
        }
        private void UpdatePath()
        {
            label2.Text = g_Path;
        }
        private void DownloadFile(string remoteFilePath, string remoteFileName)
        {        
            string localPath = @"C:\uNotes\" + remoteFileName;
            string remoteFile = "";

            if (remoteFilePath != "")
            {
                remoteFile = remoteFilePath + "/" + remoteFileName;
            }
            else
            {
                remoteFile= remoteFileName;
            }

            //MessageBox.Show(localPath + " remote: " + remoteFile);
            kliens.DownloadFile(localPath, remoteFile, true);
        }
        private void listBox2_DoubleClick(object sender, EventArgs e)
        {
            DownloadFile(g_Path, GetSelectedItemListBox2());
        }

        private void listBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DownloadFile(g_Path, GetSelectedItemListBox2());
            }
        }
    }
}
